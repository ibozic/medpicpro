function varargout = dynamic_study(varargin)
% DYNAMIC_STUDY M-file for dynamic_study.fig
%      DYNAMIC_STUDY, by itself, creates a new DYNAMIC_STUDY or raises the existing
%      singleton*.
%
%      H = DYNAMIC_STUDY returns the handle to a new DYNAMIC_STUDY or the handle to
%      the existing singleton*.
%
%      DYNAMIC_STUDY('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DYNAMIC_STUDY.M with the given input arguments.
%
%      DYNAMIC_STUDY('Property','Value',...) creates a new DYNAMIC_STUDY or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dynamic_study_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dynamic_study_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dynamic_study

% Last Modified by GUIDE v2.5 18-Mar-2008 12:25:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dynamic_study_OpeningFcn, ...
                   'gui_OutputFcn',  @dynamic_study_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dynamic_study is made visible.
function dynamic_study_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dynamic_study (see VARARGIN)

% Choose default command line output for dynamic_study
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes dynamic_study wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dynamic_study_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider



% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

handles.v1=(get(hObject,'String'));
handles.v1=str2double(handles.v1);
handles.px=1;

if (isnan(handles.v1) && ~(isinteger(handles.v1)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit1,'String','');
    set(handles.v1,'Value',0);
    handles.px=0;
    return

else
end

if (handles.px==1 && handles.py==1)
    
set(handles.pushbutton2,'Enable','on');
set(handles.slider1,'Enable','on');
    
end    

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

handles.v2=(get(hObject,'String'));
handles.v2=str2double(handles.v2);
handles.py=1;

if (isnan(handles.v2) && ~(isinteger(handles.v2)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit2,'String','');
    set(handles.v2,'Value',0);
    handles.py=0;
    return

else
end

if (handles.px==1 && handles.py==1)
    
set(handles.pushbutton2,'Enable','on');
set(handles.slider1,'Enable','on');
    
end    

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double

handles.v3=(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double

handles.v4=(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


if handles.P==1
    
    set(handles.pushbutton3,'Enable','on');
    set(handles.pushbutton4,'Enable','on');
    
for i=handles.v1:handles.v2
    handles.stop=get(handles.pushbutton4,'Value');
    handles.pausex=get(handles.pushbutton3,'Value');
    handles.slider=get(handles.slider1,'Value');
    set(handles.pushbutton2,'Enable','off');
    handles.pause=handles.slider;
    handles.time=(i/25);
    set(handles.edit5,'String',handles.time);
    handles.filename=[handles.v3, num2str(i,'%2d'),handles.v4];
    handles.temp=(imread(handles.filename));
   
    if handles.stop==1
        
        handles.filename1=[handles.v3, num2str(handles.v1,'%2d'),handles.v4];
        handles.temp1=(imread(handles.filename1));
        imshow(handles.temp1);
        set(handles.edit5,'String','');
        i=handles.v2;
        set(handles.pushbutton4,'Value',0);
        set(handles.pushbutton2,'Enable','on');
        guidata(hObject,handles);
        break
  
    else
     
    imshow(handles.temp);colorbar('vert');
    drawnow;pause(handles.pause);
    guidata(hObject,handles);
    
       
   end
   
      if handles.pausex==1
%     handles.pause1=100;   
    imshow(handles.temp);
    set(handles.pushbutton3,'Value',0);
    set(handles.pushbutton3,'Enable','off');
    set(handles.pushbutton3,'String','Press any key to continue');
    set(handles.pushbutton2,'Enable','off');
    drawnow; pause;
%     handles.pausex=get(handles.pushbutton3,'Value');


    guidata(hObject,handles); 
      
      else

    imshow(handles.temp);
    drawnow; pause(handles.pause1);
    guidata(hObject,handles);
    set(handles.pushbutton3,'Enable','on');
    set(handles.pushbutton3,'String','Pause');
    
       
   end
   guidata(hObject,handles);
set(handles.pushbutton2,'Enable','on');
end    

elseif handles.P==-1
    
    set(handles.pushbutton3,'Enable','on');
    set(handles.pushbutton4,'Enable','on');
    
for i=handles.v1:handles.v2
    handles.slider=get(handles.slider1,'Value');
    handles.pause=handles.slider;
    handles.time=(i/25);
    set(handles.edit5,'String',handles.time);
    handles.filename=[handles.v3, num2str(i,'%2d'),handles.v4];
    handles.temp=(dicomread(handles.filename));
    imshow(handles.temp,[]);
    drawnow; pause(handles.pause);
    guidata(hObject,handles);
end    
end

guidata(hObject,handles);

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.butt=get(hObject,'Value');
if handles.butt==get(hObject,'Max')
    
    handles.pausex=1;
%     handles.pause1=100;
    
else handles.pause1=0;
     handles.pausex=0;
end
    
    guidata(hObject,handles);
    

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function pic_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to pic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.P=1;
handles.pause=0;
handles.time=0;
handles.px=0;
handles.py=0;
handles.pausex=0;
handles.pause1=0;
handles.stop=0;
set(handles.text1,'Visible','on');
set(handles.text2,'Visible','on');
set(handles.text3,'Visible','on');
set(handles.text4,'Visible','on');
set(handles.text5,'Visible','on');
set(handles.text6,'Visible','on');
set(handles.text7,'Visible','on');
set(handles.text8,'Visible','on');
set(handles.edit1,'Visible','on');
set(handles.edit2,'Visible','on');
set(handles.edit3,'Visible','on');
set(handles.edit4,'Visible','on');
set(handles.edit5,'Visible','on');
set(handles.pushbutton2,'Visible','on');
set(handles.pushbutton3,'Visible','on');
set(handles.pushbutton4,'Visible','on');
set(handles.slider1,'Visible','on');
set(handles.pushbutton2,'Enable','off');
set(handles.pushbutton3,'Enable','off');
set(handles.pushbutton4,'Enable','off');
set(handles.slider1,'Enable','off');
guidata(hObject,handles);


% --------------------------------------------------------------------
function dicom_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to dicom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.P=-1;
handles.pause=0;
handles.time=0;
handles.px=0;
handles.py=0;
handles.stop=0;
set(handles.text1,'Visible','on');
set(handles.text2,'Visible','on');
set(handles.text3,'Visible','on');
set(handles.text4,'Visible','on');
set(handles.text5,'Visible','on');
set(handles.text6,'Visible','on');
set(handles.text7,'Visible','on');
set(handles.text8,'Visible','on');
set(handles.edit1,'Visible','on');
set(handles.edit2,'Visible','on');
set(handles.edit3,'Visible','on');
set(handles.edit4,'Visible','on');
set(handles.edit5,'Visible','on');
set(handles.pushbutton2,'Visible','on');
set(handles.pushbutton3,'Visible','on');
set(handles.pushbutton4,'Visible','on');
set(handles.pushbutton2,'Enable','off');
set(handles.pushbutton3,'Enable','off');
set(handles.pushbutton4,'Enable','off');
set(handles.slider1,'Visible','on');
set(handles.slider1,'Enable','off');
guidata(hObject,handles);


% --------------------------------------------------------------------
function uipushtool6_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

run('Main_Window');
delete(handles.figure1);
