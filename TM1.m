function varargout = TM1(varargin)
% TM1 M-file for TM1.fig
%      TM1, by itself, creates a new TM1 or raises the existing
%      singleton*.
%
%      H = TM1 returns the handle to a new TM1 or the handle to
%      the existing singleton*.
%
%      TM1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TM1.M with the given input arguments.
%
%      TM1('Property','Value',...) creates a new TM1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TM1_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TM1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TM1

% Last Modified by GUIDE v2.5 04-Jun-2008 02:53:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TM1_OpeningFcn, ...
                   'gui_OutputFcn',  @TM1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TM1 is made visible.
function TM1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TM1 (see VARARGIN)

% Choose default command line output for TM1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TM1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = TM1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double

handles.v4=(get(hObject,'String'));
handles.v4=str2double(handles.v4);

if (isnan(handles.v4) && ~(isinteger(handles.v4)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit4,'String','');
    handles.v4=(get(hObject,'String'));
    handles.v4=str2double(handles.v4);
    return

else 
    
   set(handles.edit4,'Enable','off');
   set(handles.pushbutton5,'Enable','on');
   set(handles.pushbutton6,'Enable','on');
   set(handles.pushbutton7,'Enable','on');
   set(handles.slider1,'Enable','on');
   set(handles.edit3,'Enable','inactive');
    
end


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1

handles.button_state = get(hObject,'Value');

if handles.button_state == get(hObject,'Max')
    
    set(handles.togglebutton2,'Enable','off');
    
elseif handles.button_state == get(hObject,'Min');    
    
    set(handles.togglebutton2,'Enable','on'); 
    
end

guidata(hObject,handles);

% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.button_state1 = get(hObject,'Value');

if handles.button_state1 == get(hObject,'Max')
    
    set(handles.uipanel4,'Visible','on');
    set(handles.radiobutton4,'Value',0);
    set(handles.radiobutton5,'Value',0);
    set(handles.radiobutton6,'Value',0);
    
elseif handles.button_state1 == get(hObject,'Min');    
    
    set(handles.uipanel4,'Visible','off');
    set(handles.uipanel5,'Visible','off');
    set(handles.radiobutton4,'Value',0);
    set(handles.radiobutton5,'Value',0);
    set(handles.radiobutton6,'Value',0);
    
end

guidata(hObject,handles);


% --- Executes on button press in radiobutton7.
function radiobutton7_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton7

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.pushbutton16,'Enable','on');
   set(handles.uipanel7,'Visible','off');
   
else
    
   set(handles.pushbutton16,'Enable','off');
    
end

% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton8

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.pushbutton16,'Enable','on');
   set(handles.uipanel7,'Visible','off');
   
else
    
   set(handles.pushbutton16,'Enable','off');
    
end

% --- Executes on button press in radiobutton9.
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton9

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.pushbutton16,'Enable','on');
   set(handles.uipanel7,'Visible','off');
   
else
    
   set(handles.pushbutton16,'Enable','off');
    
end

% --- Executes on button press in radiobutton10.
function radiobutton10_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton10

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.pushbutton16,'Enable','on');
   set(handles.uipanel7,'Visible','off');
   
else
    
   set(handles.pushbutton16,'Enable','off');
    
end

% --- Executes on button press in radiobutton11.
function radiobutton11_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton11

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.pushbutton16,'Enable','on');
   set(handles.uipanel7,'Visible','off');
   
else
    
   set(handles.pushbutton16,'Enable','off');
    
end

% --- Executes on button press in radiobutton12.
function radiobutton12_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton12

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel7,'Visible','on');
   set(handles.pushbutton16,'Enable','off');
   set(handles.checkbox2,'Value',0);
   set(handles.edit5,'String','');
   set(handles.edit6,'String','');
   set(handles.checkbox1,'Enable','off');
   set(handles.checkbox1,'Value',0);
   
else 
    
   set(handles.uipanel7,'Visible','off');
   set(handles.pushbutton16,'Enable','off');
  
end

function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double

handles.v5=(get(hObject,'String'));
handles.v5=str2double(handles.v5);

if (isnan(handles.v5) && ~(isinteger(handles.v5)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit5,'String','');
    set(handles.v5,'Value',0);
    return

else
end

if (handles.v5>0 && handles.v6>0)
    
    set(handles.checkbox1,'Enable','on');
    
else
end

guidata(hObject,handles);



% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double

handles.v6=(get(hObject,'String'));
handles.v6=str2double(handles.v6);

if (isnan(handles.v6) && ~(isinteger(handles.v6)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit6,'String','');
    set(handles.v6,'Value',0);
    return

else
end

if (handles.v5>0 && handles.v6>0)
    
    set(handles.checkbox1,'Enable','on');
    
else
end

guidata(hObject,handles);




% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel7,'Visible','off');
   set(handles.pushbutton16,'Enable','on');
   set(handles.radiobutton12,'Value',0);
   
else 
end

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel7,'Visible','off');
   set(handles.pushbutton16,'Enable','off');
   set(handles.radiobutton12,'Value',0);
   
else 
end

% --- Executes on button press in radiobutton19.
function radiobutton19_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton19


% --- Executes on button press in radiobutton20.
function radiobutton20_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton20


% --- Executes on button press in radiobutton21.
function radiobutton21_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton21


% --- Executes on button press in radiobutton22.
function radiobutton22_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton22


% --- Executes on button press in radiobutton23.
function radiobutton23_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton23


% --- Executes on button press in radiobutton24.
function radiobutton24_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton24



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double

handles.v12=(get(hObject,'String'));
handles.v12=str2double(handles.v12);

if (isnan(handles.v12) && ~(isinteger(handles.v12)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit12,'String','');
    set(handles.v12,'Value',0);
    return

else
end

if (handles.v12>0 && handles.v13>0)
    
    set(handles.checkbox5,'Enable','on');
    
else
end

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


handles.v13=(get(hObject,'String'));
handles.v13=str2double(handles.v13);

if (isnan(handles.v13) && ~(isinteger(handles.v13)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit13,'String','');
    set(handles.v13,'Value',0);
    return

else
end

if (handles.v13>0 && handles.v13>0)
    
    set(handles.checkbox5,'Enable','on');
    
else
end

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel13,'Visible','off');
   set(handles.uipanel19,'Visible','on');
   set(handles.pushbutton19,'Enable','off');
   set(handles.radiobutton32,'Value',0);
   
else 
end

% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel7,'Visible','off');
   set(handles.pushbutton19,'Enable','off');
   set(handles.radiobutton32,'Value',0);
   
else 
end

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in radiobutton25.
function radiobutton25_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton25

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.pushbutton14,'Enable','on');
   
else
    
   set(handles.pushbutton14,'Enable','off');
    
end

% --- Executes on button press in radiobutton26.
function radiobutton26_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton26

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.pushbutton14,'Enable','on');
   
else
    
   set(handles.pushbutton14,'Enable','off');
    
end

% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

   set(handles.uipanel19,'Visible','off');
   set(handles.pushbutton19,'Enable','on');
   set(handles.radiobutton27,'Enable','off');
   set(handles.radiobutton28,'Enable','off');
   set(handles.radiobutton29,'Enable','off');
   set(handles.radiobutton30,'Enable','off');
   set(handles.radiobutton31,'Enable','off');
   set(handles.radiobutton32,'Enable','off');
   set(handles.radiobutton27,'Value',0);
   set(handles.radiobutton28,'Value',0);
   set(handles.radiobutton29,'Value',0);
   set(handles.radiobutton30,'Value',0);
   set(handles.radiobutton31,'Value',0);
   set(handles.radiobutton32,'Value',0);

% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

   set(handles.uipanel19,'Visible','off');
   set(handles.radiobutton27,'Value',0);
   set(handles.radiobutton28,'Value',0);
   set(handles.radiobutton29,'Value',0);
   set(handles.radiobutton30,'Value',0);
   set(handles.radiobutton31,'Value',0);
   set(handles.radiobutton32,'Value',0);

% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

if (get(hObject,'Value') == get(hObject,'Max'))
    
  set(handles.uipanel2,'Visible','on');
  set(handles.uipanel3,'Visible','off');
  set(handles.uipanel11,'Visible','off');
  set(handles.uipanel4,'Visible','off');
  set(handles.uipanel5,'Visible','off');
  set(handles.uipanel7,'Visible','off');
  set(handles.uipanel13,'Visible','off');
  set(handles.uipanel19,'Visible','off');
  set(handles.edit7,'Visible','off');
  set(handles.text9,'Visible','off');
  set(handles.slider2,'Visible','off');
  set(handles.edit11,'Visible','off');
  set(handles.text14,'Visible','off');
  set(handles.slider4,'Visible','off');
  set(handles.radiobutton4,'Value',0);
  set(handles.radiobutton5,'Value',0);
  set(handles.radiobutton6,'Value',0);
  set(handles.radiobutton7,'Value',0);
  set(handles.radiobutton8,'Value',0);
  set(handles.radiobutton9,'Value',0);
  set(handles.radiobutton10,'Value',0);
  set(handles.radiobutton11,'Value',0);
  set(handles.radiobutton12,'Value',0);
  set(handles.radiobutton25,'Value',0);
  set(handles.radiobutton26,'Value',0);
  set(handles.radiobutton27,'Value',0);
  set(handles.radiobutton28,'Value',0);
  set(handles.radiobutton29,'Value',0);
  set(handles.radiobutton30,'Value',0);
  set(handles.radiobutton31,'Value',0);
  set(handles.radiobutton32,'Value',0);
  set(handles.radiobutton7,'Enable','on');
  set(handles.radiobutton8,'Enable','on');
  set(handles.radiobutton9,'Enable','on');
  set(handles.radiobutton10,'Enable','on');
  set(handles.radiobutton11,'Enable','on');
  set(handles.radiobutton12,'Enable','on');
  set(handles.radiobutton27,'Enable','on');
  set(handles.radiobutton28,'Enable','on');
  set(handles.radiobutton29,'Enable','on');
  set(handles.radiobutton30,'Enable','on');
  set(handles.radiobutton31,'Enable','on');
  set(handles.radiobutton32,'Enable','on');
  set(handles.checkbox1,'Value',0);
  set(handles.checkbox2,'Value',0);
  set(handles.checkbox5,'Value',0);
  set(handles.checkbox6,'Value',0);
  set(handles.edit3,'String','');
  set(handles.edit4,'String','');
  set(handles.edit5,'String','');
  handles.v5=(get(hObject,'String'));
  handles.v5=str2double(handles.v5);
  set(handles.edit6,'String','');
  handles.v6=(get(hObject,'String'));
  handles.v6=str2double(handles.v6);
  set(handles.edit7,'String','');
  set(handles.edit12,'String','');
  handles.v12=(get(hObject,'String'));
  handles.v12=str2double(handles.v12);
  set(handles.edit13,'String','');
  handles.v13=(get(hObject,'String'));
  handles.v13=str2double(handles.v13);
  set(handles.edit11,'String','');
  set(handles.togglebutton2,'Enable','off');
  set(handles.pushbutton8,'Enable','off');
  set(handles.pushbutton9,'Enable','off');
  set(handles.pushbutton16,'Enable','off');
  set(handles.pushbutton17,'Enable','off');
  set(handles.pushbutton18,'Enable','off');
  set(handles.pushbutton19,'Enable','off');
  set(handles.pushbutton14,'Enable','off');
  set(handles.checkbox1,'Enable','off');
  set(handles.checkbox5,'Enable','off');
  
else

    set(handles.uipanel4,'Visible','off');

end

guidata(hObject,handles);

% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2

if (get(hObject,'Value') == get(hObject,'Max'))
    
  set(handles.uipanel2,'Visible','off');
  set(handles.uipanel3,'Visible','off');
  set(handles.uipanel11,'Visible','on');
  set(handles.uipanel4,'Visible','off');
  set(handles.uipanel5,'Visible','off');
  set(handles.uipanel7,'Visible','off');
  set(handles.uipanel13,'Visible','off');
  set(handles.uipanel19,'Visible','off');
  set(handles.edit7,'Visible','off');
  set(handles.text9,'Visible','off');
  set(handles.slider2,'Visible','off');
  set(handles.edit11,'Visible','off');
  set(handles.text14,'Visible','off');
  set(handles.slider4,'Visible','off');
  set(handles.radiobutton4,'Value',0);
  set(handles.radiobutton5,'Value',0);
  set(handles.radiobutton6,'Value',0);
  set(handles.radiobutton7,'Value',0);
  set(handles.radiobutton8,'Value',0);
  set(handles.radiobutton9,'Value',0);
  set(handles.radiobutton10,'Value',0);
  set(handles.radiobutton11,'Value',0);
  set(handles.radiobutton12,'Value',0);
  set(handles.radiobutton25,'Value',0);
  set(handles.radiobutton26,'Value',0);
  set(handles.radiobutton27,'Value',0);
  set(handles.radiobutton28,'Value',0);
  set(handles.radiobutton29,'Value',0);
  set(handles.radiobutton30,'Value',0);
  set(handles.radiobutton31,'Value',0);
  set(handles.radiobutton32,'Value',0);
  set(handles.radiobutton7,'Enable','on');
  set(handles.radiobutton8,'Enable','on');
  set(handles.radiobutton9,'Enable','on');
  set(handles.radiobutton10,'Enable','on');
  set(handles.radiobutton11,'Enable','on');
  set(handles.radiobutton12,'Enable','on');
  set(handles.radiobutton27,'Enable','on');
  set(handles.radiobutton28,'Enable','on');
  set(handles.radiobutton29,'Enable','on');
  set(handles.radiobutton30,'Enable','on');
  set(handles.radiobutton31,'Enable','on');
  set(handles.radiobutton32,'Enable','on');
  set(handles.checkbox1,'Value',0);
  set(handles.checkbox2,'Value',0);
  set(handles.checkbox5,'Value',0);
  set(handles.checkbox6,'Value',0);
  set(handles.edit3,'String','');
  set(handles.edit4,'String','');
  set(handles.edit5,'String','');
  handles.v5=(get(hObject,'String'));
  handles.v5=str2double(handles.v5);
  set(handles.edit6,'String','');
  handles.v6=(get(hObject,'String'));
  handles.v6=str2double(handles.v6);
  set(handles.edit7,'String','');
  set(handles.edit12,'String','');
  handles.v12=(get(hObject,'String'));
  handles.v12=str2double(handles.v12);
  set(handles.edit13,'String','');
  handles.v13=(get(hObject,'String'));
  handles.v13=str2double(handles.v13);
  set(handles.edit11,'String','');
  set(handles.togglebutton2,'Enable','off');
  set(handles.pushbutton8,'Enable','off');
  set(handles.pushbutton9,'Enable','off');
  set(handles.pushbutton16,'Enable','off');
  set(handles.pushbutton17,'Enable','off');
  set(handles.pushbutton18,'Enable','off');
  set(handles.pushbutton19,'Enable','off');
  set(handles.pushbutton14,'Enable','off');
  set(handles.checkbox1,'Enable','off');
  set(handles.checkbox5,'Enable','off');

else

    set(handles.uipanel2,'Visible','off');

end

guidata(hObject,handles);

% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3


if (get(hObject,'Value') == get(hObject,'Max'))
    
  set(handles.uipanel2,'Visible','off');
  set(handles.uipanel3,'Visible','on');
  set(handles.uipanel11,'Visible','off');
  set(handles.uipanel4,'Visible','off');
  set(handles.uipanel5,'Visible','off');
  set(handles.uipanel7,'Visible','off');
  set(handles.uipanel13,'Visible','off');
  set(handles.uipanel19,'Visible','off');
  set(handles.edit7,'Visible','off');
  set(handles.text9,'Visible','off');
  set(handles.slider2,'Visible','off');
  set(handles.edit11,'Visible','off');
  set(handles.text14,'Visible','off');
  set(handles.slider4,'Visible','off');
  set(handles.radiobutton4,'Value',0);
  set(handles.radiobutton5,'Value',0);
  set(handles.radiobutton6,'Value',0);
  set(handles.radiobutton7,'Value',0);
  set(handles.radiobutton8,'Value',0);
  set(handles.radiobutton9,'Value',0);
  set(handles.radiobutton10,'Value',0);
  set(handles.radiobutton11,'Value',0);
  set(handles.radiobutton12,'Value',0);
  set(handles.radiobutton25,'Value',0);
  set(handles.radiobutton26,'Value',0);
  set(handles.radiobutton27,'Value',0);
  set(handles.radiobutton28,'Value',0);
  set(handles.radiobutton29,'Value',0);
  set(handles.radiobutton30,'Value',0);
  set(handles.radiobutton31,'Value',0);
  set(handles.radiobutton32,'Value',0);
  set(handles.radiobutton7,'Enable','on');
  set(handles.radiobutton8,'Enable','on');
  set(handles.radiobutton9,'Enable','on');
  set(handles.radiobutton10,'Enable','on');
  set(handles.radiobutton11,'Enable','on');
  set(handles.radiobutton12,'Enable','on');
  set(handles.radiobutton27,'Enable','on');
  set(handles.radiobutton28,'Enable','on');
  set(handles.radiobutton29,'Enable','on');
  set(handles.radiobutton30,'Enable','on');
  set(handles.radiobutton31,'Enable','on');
  set(handles.radiobutton32,'Enable','on');
  set(handles.checkbox1,'Value',0);
  set(handles.checkbox2,'Value',0);
  set(handles.checkbox5,'Value',0);
  set(handles.checkbox6,'Value',0);
  set(handles.edit3,'String','');
  set(handles.edit4,'String','');
  set(handles.edit5,'String','');
  handles.v5=(get(hObject,'String'));
  handles.v5=str2double(handles.v5);
  set(handles.edit6,'String','');
  handles.v6=(get(hObject,'String'));
  handles.v6=str2double(handles.v6);
  set(handles.edit7,'String','');
  set(handles.edit12,'String','');
  handles.v12=(get(hObject,'String'));
  handles.v12=str2double(handles.v12);
  set(handles.edit13,'String','');
  handles.v13=(get(hObject,'String'));
  handles.v13=str2double(handles.v13);
  set(handles.edit11,'String','');
  set(handles.togglebutton2,'Enable','off');
  set(handles.pushbutton8,'Enable','off');
  set(handles.pushbutton9,'Enable','off');
  set(handles.pushbutton16,'Enable','off');
  set(handles.pushbutton17,'Enable','off');
  set(handles.pushbutton18,'Enable','off');
  set(handles.pushbutton19,'Enable','off');
  set(handles.pushbutton14,'Enable','off');
  set(handles.checkbox1,'Enable','off');
  set(handles.checkbox5,'Enable','off');
    
else

    set(handles.uipanel3,'Visible','off');

end

guidata(hObject,handles);

% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel5,'Visible','on');
   set(handles.edit4,'Enable','off');
   set(handles.pushbutton5,'Enable','on');
   set(handles.pushbutton6,'Enable','on');
   set(handles.pushbutton7,'Enable','on');
   set(handles.slider1,'Enable','on');
   set(handles.edit3,'Enable','inactive');
   set(handles.edit4,'String','');
   
else 
    
   set(handles.uipanel5,'Visible','off');
   
end


% --- Executes on button press in radiobutton6.
function radiobutton6_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton6

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel5,'Visible','on');
   set(handles.edit4,'Enable','on');
   set(handles.pushbutton5,'Enable','off');
   set(handles.pushbutton6,'Enable','off');
   set(handles.pushbutton7,'Enable','off');
   set(handles.slider1,'Enable','off');
   set(handles.edit3,'Enable','off');
   set(handles.edit4,'String','');
   
else 
    
   set(handles.uipanel5,'Visible','off');
   set(handles.edit4,'String','');
   
end


% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton4

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel5,'Visible','off');
   set(handles.edit4,'Enable','off');
   
else 
end


% --- Executes on button press in radiobutton32.
function radiobutton32_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton32

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel13,'Visible','on');
   set(handles.uipanel19,'Visible','off');
   set(handles.pushbutton19,'Enable','off');
   set(handles.checkbox6,'Value',0);
   set(handles.edit12,'String','');
   set(handles.edit13,'String','');
   set(handles.checkbox5,'Enable','off');
   set(handles.checkbox5,'Value',0);
   
else 
    
   set(handles.uipanel13,'Visible','off');
   set(handles.pushbutton19,'Enable','off');
  
end


% --- Executes on button press in pushbutton17.
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

  set(handles.edit11,'Visible','on');
  set(handles.text14,'Visible','on');
  set(handles.slider4,'Visible','on');

% --- Executes on button press in pushbutton16.
function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

  set(handles.edit7,'Visible','on');
  set(handles.text9,'Visible','on');
  set(handles.slider2,'Visible','on');
  set(handles.radiobutton7,'Enable','off');
  set(handles.radiobutton8,'Enable','off');
  set(handles.radiobutton9,'Enable','off');
  set(handles.radiobutton10,'Enable','off');
  set(handles.radiobutton11,'Enable','off');
  set(handles.radiobutton12,'Enable','off');


% --- Executes on button press in radiobutton27.
function radiobutton27_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton27

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel19,'Visible','on');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
   set(handles.uipanel13,'Visible','off');
   
else
    
   set(handles.uipanel19,'Visible','off');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
    
end


% --- Executes on button press in radiobutton28.
function radiobutton28_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton28


if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel19,'Visible','on');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
   set(handles.uipanel13,'Visible','off');
   
else
    
   set(handles.uipanel19,'Visible','off');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
    
end


% --- Executes on button press in radiobutton29.
function radiobutton29_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton29

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel19,'Visible','on');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
   set(handles.uipanel13,'Visible','off');
   
else
    
   set(handles.uipanel19,'Visible','off');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
   
end



% --- Executes on button press in radiobutton30.
function radiobutton30_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton30

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel19,'Visible','on');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
   set(handles.uipanel13,'Visible','off');
   
else
    
   set(handles.uipanel19,'Visible','off');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
      
end



% --- Executes on button press in radiobutton31.
function radiobutton31_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton31

if (get(hObject,'Value') == get(hObject,'Max'))
    
   set(handles.uipanel19,'Visible','on');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
   set(handles.uipanel13,'Visible','off');
   
else
    
   set(handles.uipanel19,'Visible','off');
   set(handles.pushbutton14,'Enable','off');
   set(handles.radiobutton25,'Value',0);
   set(handles.radiobutton26,'Value',0);
   
end


