function varargout = triDviz(varargin)
% TRIDVIZ M-file for triDviz.fig
%      TRIDVIZ, by itself, creates a new TRIDVIZ or raises the existing
%      singleton*.
%
%      H = TRIDVIZ returns the handle to a new TRIDVIZ or the handle to
%      the existing singleton*.
%
%      TRIDVIZ('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRIDVIZ.M with the given input arguments.
%
%      TRIDVIZ('Property','Value',...) creates a new TRIDVIZ or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before triDviz_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to triDviz_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help triDviz

% Last Modified by GUIDE v2.5 18-Mar-2008 12:31:12

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @triDviz_OpeningFcn, ...
                   'gui_OutputFcn',  @triDviz_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before triDviz is made visible.
function triDviz_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to triDviz (see VARARGIN)

% Choose default command line output for triDviz
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes triDviz wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = triDviz_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%build the surface of constant fluro intensity
axes(handles.axes2);
p=patch(isosurface(handles.slice12,50));
reducepatch(p, .5);
set(p,'facecolor',[1,1,1],'edgecolor','none');
set(gca,'projection','perspective');
box on
light('position',[1,1,1]);
light('position',[-1,-1,-1]);

%scale in the z direcion to compensate
%for slice thickness
%zscale=.05;  % direktno odregjuje debljinu slajsa, sto je manje slajs je deblji
daspect('auto');
set(gca,'color',[1,1,1]*.8);
set(gca,'xlim',[0 550], 'ylim',[0 550]);
az=0;
el=0;
view(az,el)
guidata(hObject,handles);

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

handles.v1=(get(hObject,'String'));
handles.v1=str2double(handles.v1);
handles.px=1;

if (isnan(handles.v1) && ~(isinteger(handles.v1)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit1,'String','');
    set(handles.v1,'Value',0);
    handles.px=0;
    return

else
end

if (handles.px==1 && handles.py==1)
    
set(handles.pushbutton2,'Enable','on');
set(handles.slider1,'Enable','on');
    
end    

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double

handles.v2=(get(hObject,'String'));
handles.v2=str2double(handles.v2);
handles.py=1;

if (isnan(handles.v2) && ~(isinteger(handles.v2)))
    
    errordlg('You Must eneter a numeric integer value','Bad Input','Modal');
    set(handles.edit2,'String','');
    set(handles.v2,'Value',0);
    handles.py=0;
    return

else
end

if (handles.px==1 && handles.py==1)
    
set(handles.pushbutton2,'Enable','on');
set(handles.slider1,'Enable','on');
    
end    

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double

handles.v3=(get(hObject,'String'));
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double

handles.v4=(get(hObject,'String'));
guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.P==1
    
for i=handles.v1:handles.v2
    axes(handles.axes1);
    handles.slider=get(handles.slider1,'Value');
    handles.pause=handles.slider;
    handles.filename=[handles.v3, num2str(i,'%2d'),handles.v4];
    handles.temp=(imread(handles.filename));
    handles.slice12(:,:,i) =handles.temp;
    imshow(handles.temp);
    drawnow; pause(handles.pause);
    guidata(hObject,handles);
end

    set(handles.pushbutton1,'Enable','on')
   
elseif handles.P==-1
    
for i=handles.v1:handles.v2
    axes(handles.axes1)
    handles.slider=get(handles.slider1,'Value');
    handles.pause=handles.slider;
    handles.filename=[handles.v3, num2str(i,'%2d'),handles.v4];
    handles.temp=(dicomread(handles.filename));
%     handles.temp=edge(handles.temp,'canny');
%     handles.temp=bwmorph(handles.temp,'dilate');
%     handles.temp=bwmorph(handles.temp,'erode');
%     handles.temp=bwmorph(handles.temp,'skel');
%     [BB,LL]=bwboundaries(handles.temp,8);
%     hold on
%  for k = 1:length(BB)
%     boundary = BB{k};
%     plot(boundary(:,2), boundary(:,1), 'w', 'LineWidth', 2)
%  end
%     hold off
%     handles.slice12(:,:,handles.v2-i+1) =handles.temp;
%     imagesc(label2rgb(LL, @jet, [.5 .5 .5]))
    imshow(handles.temp,[]);
    drawnow; pause(handles.pause);
    guidata(hObject,handles);
end    

 set(handles.pushbutton1,'Enable','on')
 
end

guidata(hObject,handles);


% --------------------------------------------------------------------
function uipushtool1_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.P=1;
handles.pause=0;
handles.time=0;
handles.px=0;
handles.py=0;
handles.v4='';
handles.v3='';
set(handles.text1,'Visible','on');
set(handles.text2,'Visible','on');
set(handles.text3,'Visible','on');
set(handles.text4,'Visible','on');
set(handles.text5,'Visible','on');
set(handles.text6,'Visible','on');
set(handles.text7,'Visible','on');
set(handles.edit1,'Visible','on');
set(handles.edit2,'Visible','on');
set(handles.edit3,'Visible','on');
set(handles.edit4,'Visible','on');
set(handles.pushbutton1,'Visible','on');
set(handles.pushbutton2,'Visible','on');
set(handles.slider1,'Visible','on');
set(handles.pushbutton1,'Enable','off');
set(handles.pushbutton2,'Enable','off');
set(handles.slider1,'Enable','off');
guidata(hObject,handles);


% --------------------------------------------------------------------
function uipushtool2_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.P=-1;
handles.pause=0;
handles.time=0;
handles.px=0;
handles.py=0;
handles.v4='';
handles.v3='';
set(handles.text1,'Visible','on');
set(handles.text2,'Visible','on');
set(handles.text3,'Visible','on');
set(handles.text4,'Visible','on');
set(handles.text5,'Visible','on');
set(handles.text6,'Visible','on');
set(handles.text7,'Visible','on');
set(handles.edit1,'Visible','on');
set(handles.edit2,'Visible','on');
set(handles.edit3,'Visible','on');
set(handles.edit4,'Visible','on');
set(handles.pushbutton1,'Visible','on');
set(handles.pushbutton2,'Visible','on');
set(handles.pushbutton1,'Enable','off');
set(handles.pushbutton2,'Enable','off');
set(handles.slider1,'Visible','on');
set(handles.slider1,'Enable','off');
guidata(hObject,handles);


% --------------------------------------------------------------------
function uipushtool4_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

run('Main_Window');
delete(handles.figure1);
