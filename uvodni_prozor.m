function varargout = uvodni_prozor(varargin)
% UVODNI_PROZOR M-file for uvodni_prozor.fig
%      UVODNI_PROZOR, by itself, creates a new UVODNI_PROZOR or raises the existing
%      singleton*.
%
%      H = UVODNI_PROZOR returns the handle to a new UVODNI_PROZOR or the handle to
%      the existing singleton*.
%
%      UVODNI_PROZOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UVODNI_PROZOR.M with the given input arguments.
%
%      UVODNI_PROZOR('Property','Value',...) creates a new UVODNI_PROZOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before uvodni_prozor_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to uvodni_prozor_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help uvodni_prozor

% Last Modified by GUIDE v2.5 11-Dec-2007 12:38:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @uvodni_prozor_OpeningFcn, ...
                   'gui_OutputFcn',  @uvodni_prozor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before uvodni_prozor is made visible.
function uvodni_prozor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to uvodni_prozor (see VARARGIN)

axes(handles.axes4);
imshow('3.jpg');
axes(handles.axes1);
imshow('1.jpg');
axes(handles.axes2);
imshow('58.jpg');
axes(handles.axes3);
imshow('srpskimonopol.jpg');

% Choose default command line output for uvodni_prozor
handles.output = hObject;


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes uvodni_prozor wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = uvodni_prozor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
run('Main_Window')
delete(handles.figure1)

% --- Executes on button press in close_it.
function close_it_Callback(hObject, eventdata, handles)
% hObject    handle to close_it (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete(handles.figure1);
