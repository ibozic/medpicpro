clf
clear all
%process a bunch of TIFF files into a volume
%the original images were had-colored by region to one of 7 colors
%so the scalar value on the output runs from from 0 to 7
%file setup
filebase = 'C:\Documents and Settings\Korisnik\Desktop';
startFrame = 1;
endFrame = 599;
%read frames, reduce size, show frames, and build volume
for i=startFrame:endFrame
    filename=[filebase, num2str(i,'%2d')];
    temp=double(imresize(imread(filename), 0.5));
    slice(:,:,i) = (temp(:,:,1)==255) + 2*(temp(:,:,2)==255) + 4*(temp(:,:,3)==255);
    imagesc(slice(:,:,i));
    colormap('gray')
    drawnow
end


% clf
% clear all
% %process a bunch of TIFF files into a volume
% %the original images were had-colored by region to one of 7 colors
% %so the scalar value on the output runs from from 0 to 7
% %file setup
% filebase = 'e:\Audax';
% startFrame = 1;
% endFrame = 74;
% %read frames, reduce size, show frames, and build volume
% for i=startFrame:endFrame
%     filename=[filebase, num2str(i,'%2d'),'.tif'];
%     temp=double(imresize(imread(filename), 0.5));
%     slice(:,:,i) = (temp(:,:,1)==255) + 2*(temp(:,:,2)==255) + 4*(temp(:,:,3)==255);
%     imagesc(slice(:,:,i));
%     colormap('gray')
%     drawnow
% end
